# -*- coding: utf-8 -*-
{
    'name': "contacts-rest-api",

    'summary': """
        REST API for res.partner model""",

    'description': """
        Creates the necesary http routes to interact with the res.partner model
    """,

    'author': "Éoj",
    'website': "https://gitlab.com/elotrojavi",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'contacts'],
}
