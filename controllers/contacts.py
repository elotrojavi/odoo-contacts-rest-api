from odoo import http, exceptions
from odoo.http import request
from werkzeug.wrappers.response import Response
from http import HTTPStatus
import json

DEFAULT_FIELDS = ['id', 'name', 'email']

class Contact(http.Controller):

    @http.route('/api/contacts', auth="public", methods=['GET'], csrf=False)
    def get_contacts(self, **kwargs):
        fields = kwargs.get('fields').split(',') if kwargs.get('fields') else DEFAULT_FIELDS
        contacts = request.env['res.partner'].sudo().search([])
        return json.dumps(contacts.read(fields=fields))

    @http.route('/api/contacts/<int:contact_id>', auth="public", methods=['GET'], csrf=False)
    def get_contact(self, contact_id, **kwargs):
        fields = str.split(',', kwargs.get('fields')) if kwargs.get('fields') else DEFAULT_FIELDS
        contact = request.env['res.partner'].sudo().browse(contact_id)
        contact_dict = contact.read(fields=fields)
        if len(contact_dict) > 0:
            return json.dumps(contact_dict[0])
        return Response(json.dumps({ "details": f'No cotact was found with  id {contact_id}'}), HTTPStatus.NOT_FOUND, content_type='application/json')

    @http.route('/api/contacts', auth='public', type='json', methods=['POST'], csrf=False)
    def post_contacts(self, **kwargs):
        contacts = request.env['res.partner']
        contact = contacts.sudo().search(['|', ('id', '=', kwargs.get('id')),('email', 'ilike', kwargs.get('email'))])
        if contact:
            contact.write(kwargs)
        else:
            contact = contacts.sudo().create(kwargs)
        return json.dumps(contact.read(fields={'id', 'name', 'email'}))

    @http.route('/api/contacts/<int:contact_id>', auth="public", methods=['DELETE'], csrf=False)
    def delete_contact(self, contact_id):
        try:
            request.env['res.partner'].sudo().browse(contact_id).unlink()
            return json.dumps({"details": "The contact was deleted correctly"})
        except exceptions.AccessError as e:
            return Response(json.dumps({'details': 'You have no permissions to delete this contact'}), HTTPStatus.FORBIDDEN, content_type='application/json')
        except exceptions.UserError as e:
            return Response(json.dumps({'details': 'This contact is in use and can\'t be deleted'}), HTTPStatus.BAD_REQUEST, content_type='application/json')
